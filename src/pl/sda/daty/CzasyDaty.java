package pl.sda.daty;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.util.Date;

public class CzasyDaty {
    public static void main(String[] args) {



        //stare rozwiazanie przedawnione nikt nie korzysta, ma slaby wybor operacji
        Date date = new Date();
        //data nowe i lepsze (wszystko z local)
        LocalDate date1=LocalDate.now();
        //czas
        LocalTime time1=LocalTime.now();
        //data i czas
        LocalDateTime localDateTime1=LocalDateTime.now();
        //różnica czasu precyzyjna, jesli interesuje nas roznicaw minutach lub sekundach




        //różnica czasu - mniej precyzyjna podaje różnice w miesiącach/latach/dniach
        Period period = Period.between(LocalDate.now(),date1);
        //date jest clasa deprecated , to znaczy ze jest stara klasa i powinna wyswietlac sie przekreslona by jej nie
        //uzywac


        date1=date1.plusDays(5);

        //znacznik czasowy od 1970 roku przechowywany w 64 bit long
        long timestamp=System.currentTimeMillis();
        long timestampNano=System.nanoTime();
        Timestamp timestamp2=new Timestamp(timestamp);

    }
}
