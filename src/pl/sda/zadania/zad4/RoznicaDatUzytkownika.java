package pl.sda.zadania.zad4;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class RoznicaDatUzytkownika {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Podaj pierwsza date");
        String dataStartScanner=scanner.nextLine();
        System.out.println("Podaj druga date");
        String dataEndScanner=scanner.nextLine();

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dataStart=LocalDate.parse(dataStartScanner,dateTimeFormatter);
        LocalDate dataEnd=LocalDate.parse(dataEndScanner,dateTimeFormatter);

        Period period = Period.between(dataStart,dataEnd);
        System.out.println("Różnica lat: "+period.getYears()+" miesięcy: "+period.getMonths()+" dni: "+period.getDays());
    }
}
