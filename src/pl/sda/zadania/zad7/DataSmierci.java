package pl.sda.zadania.zad7;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.Scanner;

public class DataSmierci {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj datę urodzenia:");

        String dataString = scanner.next();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateStarting = LocalDate.parse(dataString, formatter);

        LocalDate date = dateStarting.plusYears(100);

        String odpowiedz;


        do {
            System.out.println("Jesteś kobietą czy mężczyzną (m/k)?");
            odpowiedz = scanner.next();
        } while (!odpowiedz.equals("m") && !odpowiedz.equals("k"));

        if (odpowiedz.equals("m")) {
            date = date.minusYears(10);
        }

        do {
            System.out.println("Czy palisz papierosy (t/n)?");
            odpowiedz = scanner.next();
        } while (!odpowiedz.equals("t") && !odpowiedz.equals("n"));

        if (odpowiedz.equals("t")) {
            date = date.minusYears(9).minusMonths(3);
        }

        do {
            System.out.println("Czy żyjesz w stresie (t/n)?");
            odpowiedz = scanner.next();
        } while (!odpowiedz.equals("t") && !odpowiedz.equals("n"));

        if (odpowiedz.equals("t")) {
            Random random = new Random();
            int wylosowana = random.nextInt(100);
            if (wylosowana < 10) {
                date = dateStarting.plusYears(60);
            }
        }

        System.out.println("Umrzesz : " + date);


    }
}
