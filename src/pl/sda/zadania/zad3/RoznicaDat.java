package pl.sda.zadania.zad3;

import java.time.LocalDate;
import java.time.Period;

public class RoznicaDat {
    public static void main(String[] args) {

        LocalDate dataStart = LocalDate.of(2018,06,28);
        LocalDate dataEnd = LocalDate.of(2077,12,24);
        Period period = Period.between(dataStart,dataEnd);
        System.out.println("Różnica lat: "+period.getYears()+" miesięcy: "+period.getMonths()+" dni: "+period.getDays());

    }
}
