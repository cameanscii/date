package pl.sda.zadania.zad1;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class AplikacjaDoWyswietlania {
    public static void main(String[] args) {

        Scanner scanner=new Scanner(System.in);

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yy/MM/dd");
//        LocalDate dateZTekstu=LocalDate.parse(tekst,dateTimeFormatter);
//        System.out.println(dateZTekstu);


        LocalDate data =LocalDate.now();
        LocalTime time = LocalTime.now();
        LocalDateTime dateTime = LocalDateTime.now();

        //deklaracja zmiennej
        String tekst;
        do{
            System.out.println("Podaj date, time, datetime lub quit: ");
            //przypisanie wartosci do zmiennej
            tekst = scanner.nextLine();

            if(tekst.equals("date")){

                System.out.println(data.format(dateTimeFormatter));

            } else if (tekst.equals("time")) {
                System.out.println(time);

            }else if (tekst.equals("datetime")) {
                System.out.println(dateTime);
                }


// wykrzyknik przed equals robi to co !=
        }while(!tekst.equals("quit") );
    }
}
