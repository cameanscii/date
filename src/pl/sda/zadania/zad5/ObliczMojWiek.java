package pl.sda.zadania.zad5;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class ObliczMojWiek {

    public static void main(String[] args) {

        LocalDate dataStart=LocalDate.of(1990,02,21);
        LocalDate dataEnd=LocalDate.now();


        Period period = Period.between(dataStart,dataEnd);
        System.out.println("Twój wiek to: "+period.getYears()+" lat "+period.getMonths()+" miesięcy "+period.getDays()+" dni ");
    }

}
