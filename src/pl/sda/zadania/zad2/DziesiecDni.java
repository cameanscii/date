package pl.sda.zadania.zad2;

import java.time.LocalDate;

public class DziesiecDni {
    public static void main(String[] args) {


        LocalDate data = LocalDate.now();
        System.out.println("Obecna data: "+data);

        LocalDate dataPlus = data.plusDays(10);
        System.out.println("Data za dziesięć dni: "+dataPlus);

        LocalDate dataMinus = data.minusDays(10);
        System.out.println("Data wcześniejsza o 10 dni: "+dataMinus);

    }
}
